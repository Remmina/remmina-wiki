# Introduction

This manual is a work in progress. Feel free to contribute and help us to make this manual useful and pleasant to read.

Screen shots are taken and modified with [Shutter](http://shutter-project.org) 

# Remmina Introduction

Remmina is a feature rich Remote Desktop Application for GNU Linux(tm) and FreeBSD and possibly other Unixes.

## Remmina features

- Maintain a list of connection profiles, organized by groups
- Make quick connections by directly putting in the server address
- Remote desktops with higher resolutions are scrollable/scalable in both window and fullscreen mode.
- Viewport fullscreen mode: remote desktop automatically scrolls when the mouse moves over the screen edge.
- Floating toolbar in fullscreen mode, allows you to switch between modes, toggle keyboard grabbing, minimize, etc.
- Tabbed interface, optionally managed by groups.
- Tray icon, allows you to quickly access configured connection profiles
- Remote screen-shots

![Remmina main features screenshot](http://www.remmina.org/images/Remmina%20Features.png)

## How to use Remmina
The following are typical use cases
### Quick connect using RDP
Use this if you don't need or want to save your connection.
Only RDP, SSH, VNC and NX are supported in the quick connect.
![Remmina Quick connect using RDP](http://www.remmina.org/images/Remmina_quick_rdp.png)
1. Choose the protocol
2. Insert the server name
3. Click on connect
### Create a new Profile
1. Click on the "New Profile" icon
![Remmina New Profile screenshot](http://www.remmina.org/images/Remmina_newprofile.png)
2. Enter your server connection details
![New VNC Profile](http://www.remmina.org/images/Remmina_newvncprofile.png)

- You can ask Remmina to execute commands before and after the connection
- If you use Avahi, you can search available hosts as highlighted
- You can group different connections.

# Options
* Name: this is the name you gave to this connection, it's just for you to recognize it easily.
* Group:
* Protocol: here you choose the protocol type: [[RDP]], [[SFTP]], [[SSH]]
* Pre-command: what to do before starting the connection
* Post-command: what to do after closing the connection
## General
* Server: here you put the IP address of the remote machine
