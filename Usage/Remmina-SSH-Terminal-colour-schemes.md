# Using colour schemes in Remmina SSH protocol plugin

## Choosing between the standard colour schemes

You can chose a colour scheme directly in the advanced TAB of your SSH connection profile

![Chossing a colour scheme in the Remmina SSH profile](https://i.imgur.com/PeJRFQv.png)

There are 6 predefined colour schemes

## Importing a colour scheme

You can import a colour scheme from the general preferences

![Importing a color scheme - screenshot](https://i.imgur.com/tb8FBP4.png)

Several colour schemes are available to download on the [iTerm2-Color-Schemes repository on GitHub](https://github.com/mbadolato/iTerm2-Color-Schemes/tree/master/remmina). 

As soon as you have found one you like, you must set "Custom colour scheme" in the SSH profile advanced TAB.

![Custom Colour Scheme](https://i.imgur.com/gQavQZH.png) 


