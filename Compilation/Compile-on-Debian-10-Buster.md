# Quick and dirty guide for compiling remmina on Debian 10

These are instructions for people or software developers who want to contribute to the development of Remmina on Debian 10 Stretch.

By following these instructions, you will get Remmina and FreeRDP compiled under the /opt/remmina_devel/ subdir, so they will not mess up your system too much. This is ideal for testing remmina.

You will also find the uninstall instructions at the bottom of this page.

You need to execute the following commands as root or as a user with sudo privileges (member of the sudo group).

**1.** Install all packages required to build freerdp and remmina:
Execute te following commands as root
```
sudo apt install build-essential git-core libssh-dev cmake libx11-dev libxext-dev libxinerama-dev \
  libxcursor-dev libxdamage-dev libxv-dev libxkbfile-dev libasound2-dev libcups2-dev libxml2 libxml2-dev \
  libxrandr-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
  libxi-dev libavutil-dev libjson-glib-dev\
  libavcodec-dev libxtst-dev libgtk-3-dev libgcrypt20-dev  libpulse-dev \
  libvte-2.91-dev libxkbfile-dev libtelepathy-glib-dev libjpeg-dev \
  libgnutls28-dev libsecret-1-dev libavahi-ui-gtk3-dev libvncserver-dev \
  libappindicator3-dev intltool libsecret-1-dev libwebkit2gtk-4.0-dev libsystemd-dev \
  libsodium-dev libkf5wallet-dev libusb-1.0-0-dev
```
**2.** As root, remove freerdp-x11 package and all packages containing the string remmina in the package name.
```
 sudo apt purge "remmina*" "libfreerdp*" "libwinpr*" "freerdp*"
```

**3.** Create a new directory for development in your home directory, and cd into it
```
mkdir ~/remmina_devel
cd ~/remmina_devel
```
**4.** Download the latest source code of FreeRDP from its master branch
```
git clone --branch 2.0.0 https://github.com/FreeRDP/FreeRDP.git
cd FreeRDP
```
**5.** Configure FreeRDP for compilation (don't forget to include -DWITH_PULSE=ON)
```
cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_SSE2=ON -DWITH_CUPS=on -DWITH_PULSE=on -DCMAKE_INSTALL_PREFIX:PATH=/opt/remmina_devel/freerdp .
```
Please note that the above line will make FreeRDP install in /opt/remmina_devel/freerdp

**6.** Compile FreeRDP and install
```
make && sudo make install
```
**7.** Make your system dynamic loader aware of the new libraries you installed. For Ubuntu x64:
```
echo /opt/remmina_devel/freerdp/lib | sudo tee /etc/ld.so.conf.d/freerdp_devel.conf > /dev/null
sudo ldconfig
```

**8.** Create a symbolik link to the executable in /usr/local/bin
```
sudo ln -s /opt/remmina_devel/freerdp/bin/xfreerdp /usr/local/bin/
```
**9.** Test the new freerdp by connecting to a RDP host
```
xfreerdp +clipboard /sound:rate:44100,channel:2 /v:hostname /u:username
```

**10.** Now clone remmina repository to your devel dir:
```
cd ~/remmina_devel
git clone https://gitlab.com/Remmina/Remmina.git
```

**11.** Configure Remmina for compilation
```
cd Remmina
cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_KF5WALLET=on -DCMAKE_INSTALL_PREFIX:PATH=/opt/remmina_devel/remmina -DCMAKE_PREFIX_PATH=/opt/remmina_devel/freerdp --build=build .
```
**12.** Compile remmina and install it
```
make && sudo make install
```
**13.** Create a symbolik link to the the executable
```
sudo ln -s /opt/remmina_devel/remmina/bin/remmina /usr/local/bin/
```
**14.** Run remmina
```
remmina
```
Please note that icons and launcher files are not installed, so don't search for remmina using Unity Dash.

## Uninstall everything
**1.** Remove the devel directory
```
rm -rf ~/remmina_devel/
```
**2.** Remove the binary directory
```
sudo rm -rf /opt/remmina_devel/
```
**3.** Cleanup symlinks and dynamic loader
```
sudo rm /etc/ld.so.conf.d/freerdp_devel.conf /usr/local/bin/remmina /usr/local/bin/xfreerdp
sudo ldconfig
```
