These are the instructions for people who want to test the latest version of Remmina (`master` branch) on RHEL-like operating systems.

Current version of this guide was tested on vanilla CentOS 7 GNOME Desktop (specifically: [CentOS-7-x86_64-DVD-1908.iso](http://tux.rainside.sk/centos/7.7.1908/isos/x86_64/)), CentOS 8 Workstation (specifically: [CentOS-8.1.1911-x86_64-dvd1.iso](http://tux.rainside.sk/centos/8.1.1911/isos/x86_64/)) and Fedora 31 x86_64 (specifically: [Fedora-Workstation-Live-x86_64-31-1.9.iso](https://download.fedoraproject.org/pub/fedora/linux/releases/31/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-31-1.9.iso)). However, it should work on all CentOS 7+, Red Hat 7+ and Fedora 20+ versions. If you find anything not working, raise an [issue](https://gitlab.com/Remmina/Remmina/issues/new).

Note that although on CentOS 8 and Fedora, they use `dnf` package manager, there is still the `yum` symlink to `dnf`, so we decided to keep `yum` in this guide to make it usable on all of these distributions without any modifications.

If you want to uninstall the program, follow the instructions in the step 0.

**0.** It is important that no other Remmina or FreeRDP version is installed, therefore we need to remove it.
```bash
# To check if a program is installed, we can use `whereis [program(s)]`
whereis freerdp remmina

# When FreeRDP is installed from the CentOS repo, use this command to remove it
sudo yum -y remove freerdp*

# When FreeRDP is installed from source code, enter the FreeRDP source code folder
#   and run the following command
cd ${HOME}/freerdp
sudo rm $(cat install_manifest.txt)

# When Remmina is installed from the CentOS repo, use this command to remove it
sudo yum -y remove remmina*

# When Remmina is installed from source code, enter the `build` folder
#   in Remmina source code folder and run the following command
cd ${HOME}/remmina/build
sudo make uninstall

# Finally, we need to delete the empty folders of Remmina and FreeRDP
# This needs to be run for uninstallation of either programs
sudo rmdir $(find $(whereis freerdp remmina | grep -Po "^[^:]*: \K.*$") -type d | tac)
```

**1.** Add necessary repos. Note that on CentOS 7 GNOME Desktop only `epel` and `rpmfusion-free-updates` are not installed by default. I presume that `base`, `extras` and `updates` repos are installed on all version of CentOS 7.
```bash
# Get some information of the currently installed distribution
# Note: This is used to determine what repositories are need to be be installed
#   and to create the relevant URLs
distro_name="$(grep -Po '^ID="*\K[^"]*' /etc/os-release)"
distro_version="$(rpm -E %$distro_name)"

if [ "$distro_name" = 'fedora' ]; then
   distro_name_short='fedora'
else
   distro_name_short='el'
   epel_repo='epel-release'
fi

# epel (on CentOS only): required for `openjpeg2-devel`, `openssl-devel`
# rpmfusion-free-updates (on both CentOS and Fedora): required for `ffmpeg-devel`
# PowerTools (on CentOS 8+ only)
# okay (on CentOS 8+); required for `avahi-ui-devel`, `openh264`, `libopenh264-devel`, `libx264-devel`
sudo yum -y install $epel_repo \
  https://download1.rpmfusion.org/free/${distro_name_short}/rpmfusion-free-release-${distro_version}.noarch.rpm

if [ "$distro_name" = 'centos' ] && [ "$distro_version" = 8 ]; then
  sudo dnf config-manager --enable PowerTools
  sudo rpm -ivh http://repo.okay.com.mx/${distro_name}/${distro_version}/x86_64/release/okay-release-1-3.el${distro_version}.noarch.rpm

  # Disable okay repo for potential package conflicts
  sudo dnf config-manager --disable okay
fi

if [ "$distro_name" = 'fedora' ]; then
  sudo dnf config-manager --enable fedora-cisco-openh264
fi
```

**3.** Update `yum` cache and packages. This step might require a reboot.
```bash
sudo yum -y upgrade
```

**4.** Install dependencies of FreeRDP and Remmina. Note that the installation dependencies for FreeRDP and Remmina was merged. If you want to know exactly which program requires which package, see the [Dependencies](#dependencies) section at the end of this document.
```bash
sudo yum -y install alsa-lib-devel atk-devel avahi-ui-gtk3 cairo-devel cmake3 \
  cups-devel docbook-style-xsl ffmpeg-devel gcc gcc-c++ git glib2-devel gnutls-devel \
  gstreamer1-devel gstreamer1-plugins-base-devel gtk3-devel harfbuzz-devel \
  json-glib-devel libappindicator-gtk3 libappindicator-gtk3-devel libgcrypt-devel \
  libsecret-devel libSM-devel libsodium libsodium-devel libsoup-devel libssh-devel \
  libusb-devel libvncserver-devel libX11-devel libXcursor-devel libXdamage-devel \
  libXext-devel libXi-devel libXinerama-devel libxkbfile-devel libXrandr-devel libxslt \
  libxslt-devel libXtst-devel libXv-devel make ninja-build openjpeg2-devel \
  openssl-devel pango-devel pulseaudio-libs-devel vte291 vte291-devel webkitgtk4-devel \
  xmlto xorg-x11-server-utils

# On CentOS 8
if [ "$distro_name" = 'centos' ] && [ "$distro_version" = 8 ]; then
  sudo yum -y install --enablerepo=okay avahi-ui-devel openh264 libopenh264-devel libx264-devel
fi

# On Fedora
if [ "$distro_name" = 'fedora' ]; then
  # TODO: Do we really need `libx264`? Or even `openh264` (that for CentOS too)?
  sudo yum -y install avahi-ui-devel libx264 libx264-devel openh264 openh264-devel
fi
```

**5.** Clone FreeRDP and Remmina repos. You may want to modify the `${HOME}/{freerdp,remmina}` paths and the folder names.
```bash
git clone --branch 2.0.0 https://github.com/FreeRDP/FreeRDP.git ${HOME}/freerdp
git clone https://gitlab.com/Remmina/Remmina.git ${HOME}/remmina
```

**6.** Build FreeRDP.
```bash
mkdir ${HOME}/freerdp/build
cd ${HOME}/freerdp/build
# In the following line, the `DWITH_PULSE=ON` option needs to be included
# Note: `-DCMAKE_INSTALL_LIBDIR=/usr/lib64` is required when `-DCMAKE_INSTALL_PREFIX:PATH`
#       is not `/usr`; otherwise Remmina will not find the `libfreerdp*` libraries
# Note: `-DWITH_OPENH264=ON -DWITH_X264=OFF` makes FreeRDP use H264 which results
#       in extreme good quality for a quite good performance
cmake3 -DCMAKE_BUILD_TYPE=Debug -DWITH_SSE2=ON -DWITH_PULSE=ON -DWITH_CUPS=on -DWITH_WAYLAND=off \
  -DCMAKE_INSTALL_LIBDIR=/usr/lib64 -DCMAKE_INSTALL_PREFIX:PATH=/opt -DWITH_OPENH264=ON \
  -DWITH_X264=OFF ..
make && sudo make install

# If you install FreeRDP and Remmina to `/opt`, you need to add `/opt/bin` to PATH
export PATH="$PATH:/opt/bin"
echo 'export PATH="$PATH:/opt/bin"' >> ${HOME}/.bashrc

# You can test FreeRDP by connecting to an RDP host
xfreerdp +clipboard /sound:rate:44100,channel:2 /v:hostname /u:username
```

**7.** Make your system dynamic loader aware of the new libraries you installed
```bash
sudo ldconfig
```

**8.** Build Remmina.
```bash
mkdir ${HOME}/remmina/build
cd ${HOME}/remmina/build
# Note: `-DCMAKE_INSTALL_LIBDIR=/usr/lib64` is not required to successfully run Remmina,
#   but `/usr/lib64` is the proper location for the libraries; again, it is not required
#   at all when Remmina is installed to `/usr`
cmake3 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=/opt -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
  -DCMAKE_PREFIX_PATH=/opt --build=build ..
make && sudo make install
```

**9.** If Remmina is installed to a custom path (e.g. for use `/opt`), we need to create some symlinks in order to .

```bash
sudo mkdir -p /usr/share/icons/hicolor/scalable/panel /usr/share/icons/hicolor/apps

for old in /opt/share/applications/*desktop $(find /opt/share/icons -type f | grep 'svg$\|png$'); do
   new="${old/opt/usr}"
   sudo ln -s $old $new
done
```

**10.** Now you should be able to run Remmina with the following command or using the GUI.
```bash
remmina &> /dev/null &
```

## Notes

### Remmina in the system tray on GNOME 3

Current GNOME Shell versions does not include system tray, therefore vanilla GNOME will not show the Remmina system tray icon and menu. Press <kbd>Super</kbd>+<kbd>M</kbd> to see the Remmina icon on the message bar. If you want to fix this, you could install either [KStatusNotifierItem/AppIndicator Support](https://extensions.gnome.org/extension/615/appindicator-support/).

Below is a guide how to install the extension. For more information on installing the `chrome-gnome-shell` , see the [documentation](https://wiki.gnome.org/Projects/GnomeShellIntegrationForChrome/Installation)

<details>
   <summary><b>How to install [KStatusNotifierItem/AppIndicator Support](https://extensions.gnome.org/extension/615/appindicator-support/) extension</b></summary>

This method installs the extension locally (i.e. for current user only).

1. Install `chrome-gnome-shell` package.

```bash
sudo yum -y install chrome-gnome-shell
```

2. Install the browser extension. Go to [GNOME extension website](https://extensions.gnome.org) and click on _Click here to install browser extension_; or you can install it from [Firefox Browser Add-ons](https://addons.mozilla.org/en-US/firefox/addon/gnome-shell-integration/).

3. Install the [KStatusNotifierItem/AppIndicator Support](https://extensions.gnome.org/extension/615/appindicator-support/) extension by clicking on the toggle on that page (it must be on `on` position). Then confirm the installation.

4. Don't forget to re-login the GNOME. :smiley:
</details>

### Remmina in the system tray on Xfce

> :information_source: This might be outdated. If you know the current state of this issue or any workaround(s), open an [issue](https://gitlab.com/Remmina/Remmina/issues/new).

Xfce and other desktop environments without app indicator support, will not show you the system tray icon if you are executing Remmina from its compilation folder (e.g. `~/remmina`) because it contains a folder named `remmina`. For more infomation, see [Bug #1363277 on Launchpad](https://bugs.launchpad.net/libappindicator/+bug/1363277).

### Dependencies

1. If you are cloning the repositories, you need `git`.

2. FreeRDP dependencies:

```bash
# Official guide (src: https://github.com/FreeRDP/FreeRDP/wiki/Compilation [rhel based])
gcc cmake ninja-build openssl-devel libX11-devel libXext-devel libXinerama-devel \
  libXcursor-devel libXi-devel libXdamage-devel libXv-devel libxkbfile-devel \
  alsa-lib-devel cups-devel ffmpeg-devel glib2-devel libusb-devel
# We had to install these at least on CentOS 8, but they are probably required on CentOS 7 too;
#   otherwise the build failed
make gcc-c++ pulseaudio-libs-devel libXrandr-devel
# Recommended: these are required to build man pages
libxslt libxslt-devel docbook-style-xsl
# Recommended: for multimedia redirection, audio and video playback
gstreamer1-devel gstreamer1-plugins-base-devel
# Recommended: for some X11 addon
xorg-x11-server-utils
# Recommended: at build-time, FreeRDP states that this is 'required by virtual:world'
cairo-devel
```

3. Remmina dependencies (besides FreeRDP and its dependencies):

```bash
atk-devel avahi-ui-devel avahi-ui-gtk3 cmake3 gnutls-devel gtk3-devel harfbuzz-devel \
  json-glib-devel libappindicator-devel libappindicator-gtk3 libappindicator-gtk3-devel \
  libgcrypt-devel libopenh264-devel libsecret-devel libSM-devel libsodium libsodium-devel \
  libsoup-devel libssh-devel libvncserver-devel libx264-devel libXtst-devel openh264 \
  openjpeg2-devel pango-devel vte291 vte291-devel webkitgtk4-devel xmlto
```
