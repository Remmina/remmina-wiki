We have tested the compilation of Remmina on several operating systems. Unfortunately, some of these guides are outdated. If you find some issues (or even better: their solutions), raise an [issue](https://gitlab.com/Remmina/Remmina/issues/new) please. :smiley:

- [Compilation guide for Red Hat/CentOS](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compilation-guide-for-RHEL)
- [Compile on Arch Linux](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Arch-Linux)
- [Compile on Debian 10 Buster](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Debian-10-Buster)
- [Compile on Debian 9 Stretch](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Debian-9-Stretch)
- [Compile on Fedora 20 ~ 26](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Fedora-20-~-26)
- [Compile on FreeBSD](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-FreeBSD)
- [Compile on Ubuntu 14.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-14.04)
- [Compile on Ubuntu 16.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-16.04)
- [Compile on Ubuntu 18.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-18.04)
- [Compile on Ubuntu 20.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-20.04)
