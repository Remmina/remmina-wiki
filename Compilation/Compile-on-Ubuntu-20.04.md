# Quick and dirty guide for compiling remmina on ubuntu 20.04

These are instructions for people or software developers who want to contribute to the latest version of Remmina on Ubuntu 20.04.

If you are an end user and you want to install the latest version of remmina, please use the "Remmina Team Ubuntu PPA - next branch", as explained on the [homepage of the wiki](https://gitlab.com/Remmina/Remmina/wikis/home).

By following these instructions, you will get Remmina and FreeRDP compiled under the /opt/remmina_devel/ subdir, so they will not mess up your system too much. This is ideal for testing remmina.

You will also find the uninstall instructions at the bottom of this page.

**1.** Install all packages required to build freerdp and remmina:

```shell
sudo apt install build-essential git-core cmake libssl-dev libx11-dev libxext-dev libxinerama-dev \
  libxcursor-dev libxdamage-dev libxv-dev libxkbfile-dev libasound2-dev libcups2-dev libxml2 libxml2-dev \
  libxrandr-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
  libxi-dev libavutil-dev \
  libavcodec-dev libxtst-dev libgtk-3-dev libgcrypt20-dev libssh-dev libpulse-dev \
  libvte-2.91-dev libxkbfile-dev libtelepathy-glib-dev libjpeg-dev \
  libgnutls28-dev libavahi-ui-gtk3-dev libvncserver-dev \
  libappindicator3-dev intltool libsecret-1-dev libwebkit2gtk-4.0-dev libsystemd-dev \
  libsoup2.4-dev libjson-glib-dev libavresample-dev libsodium-dev \
  libusb-1.0-0-dev
```

**2.** Remove freerdp-x11 package and all packages containing the string remmina in the package name.

```shell
sudo apt purge "?name(^remmina.*)" "?name(^libfreerdp.*)" "?name(^freerdp.*)" "?name(^libwinpr.*)"
```

**3.** Create a new directory for development in your home directory, and cd into it

```shell
mkdir ~/remmina_devel
cd ~/remmina_devel
```

**4.** Download the latest source code of FreeRDP from its master branch

```shell
git clone --branch stable-2.0 https://github.com/FreeRDP/FreeRDP.git
cd FreeRDP
```

**5.** Configure FreeRDP for compilation (don't forget to include -DWITH_PULSE=ON)

```shell
cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_SSE2=ON -DWITH_CUPS=on -DWITH_PULSE=on -DCMAKE_INSTALL_PREFIX:PATH=/opt/remmina_devel/freerdp .
```

Please note that the above line will make FreeRDP install in /opt/remmina_devel/freerdp

**6.** Compile FreeRDP and install

```shell
make && sudo make install
```

**7.** Make your system dynamic loader aware of the new libraries you installed. For Ubuntu x64:

```shell
echo /opt/remmina_devel/freerdp/lib | sudo tee /etc/ld.so.conf.d/freerdp_devel.conf > /dev/null
sudo ldconfig
```

**8.** Create a symbolik link to the executable in /usr/local/bin

```shell
sudo ln -s /opt/remmina_devel/freerdp/bin/xfreerdp /usr/local/bin/
```

**9.** Test the new freerdp by connecting to a RDP host

```shell
xfreerdp +clipboard /sound:rate:44100,channel:2 /v:hostname /u:username
```

**10.** Now clone remmina repository to your devel dir:

```shell
cd ~/remmina_devel
git clone https://gitlab.com/Remmina/Remmina.git
```

**11.** Configure Remmina for compilation

```shell
cd Remmina
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=/opt/remmina_devel/remmina -DCMAKE_PREFIX_PATH=/opt/remmina_devel/freerdp --build=build .
```

**12.** Compile remmina and install it

```shell
make && sudo make install
```

**13.** Create a symbolik link to the the executable

```shell
sudo ln -s /opt/remmina_devel/remmina/bin/remmina /usr/local/bin/
```

**14.** Run remmina

```shell
remmina
```

Please note that icons and launcher files are not installed, so don't search for remmina using Unity Dash.

## Uninstall everything

**1.** Remove the devel directory

```shell
rm -rf ~/remmina_devel/
```

**2.** Remove the binary directory

```shell
sudo rm -rf /opt/remmina_devel/
```

**3.** Cleanup symlinks and dynamic loader

```shell
sudo rm /etc/ld.so.conf.d/freerdp_devel.conf /usr/local/bin/remmina /usr/local/bin/xfreerdp
sudo ldconfig
```
