Distribution|GTK+3 version|libssh version|libvncserver version|
---|---:|---:|---:
Debian 7 Wheezy|[3.4.2][gtk-wheezy]|[0.5.4][ssh-wheezy]|[0.9.9][vnc-wheezy]
Debian 8 Jessie|[3.14.5][gtk-jessie]|[0.6.3][ssh-jessie]|[0.9.9][vnc-jessie]
Debian 9 Stretch (current Raspbian)|[3.22.11][gtk-stretch]|[0.7.3][ssh-stretch]|[0.9.11][vnc-stretch]
Debian Sid|[3.22.30][gtk-sid]|[0.8.0][ssh-sid]|[0.9.11][vnc-sid]
CentOS 7/RHEL 7|[3.8][gtk-centos]|[0.6.4][ssh-centos]|[0.9.9][vnc-centos]
Fedora 26|[3.22.16][gtk-fedora]|[0.7.5][ssh-fedora]|[0.9.11][vnc-fedora]
Fedora 27|[3.22.24][gtk-fedora]|[0.7.5][ssh-fedora]|[0.9.11][vnc-fedora]
Fedora 28|[3.22.30][gtk-fedora]|[0.7.5][ssh-fedora]|[0.9.11][vnc-fedora]
Ubuntu 10.04 LTS (Lucid Lynx)|-|[0.4.2][ssh-lucid]|[0.9.7][vnc-lucid]
Ubuntu 12.04 LTS (Precise Pangolin)|[3.4.2][gtk-precise]|[0.5.2][ssh-precise]|[0.9.8.2][vnc-precise]
Ubuntu 14.04 LTS (Trusty Tahr)|[3.10.8][gtk-trusty]|[0.6.1][ssh-trusty]|[0.9.9][vnc-trusty]
Ubuntu 14.10 (Utopic Unicorn)|[3.12.2][gtk-utopic]|[0.6.3][ssh-utopic]|[0.9.9][vnc-utopic]
Ubuntu 15.04 (Vivid Vervet)|[3.14.7][gtk-vivid]|[0.6.3][ssh-vivid]|[0.9.9][vnc-vivid]
Ubuntu 16.04 (Xenial Xerus)|[3.18.9][gtk-xenial]|[0.6.3][ssh-xenial]|[0.9.10][vnc-xenial]
Ubuntu 17.04 (Zesty Zapus)|[3.22.11][gtk-zesty]|[0.7.3][ssh-zesty]|[0.9.11][vnc-zesty]
Ubuntu 18.04 (Bionic Beaver)|[3.22.30][gtk-bionic]|[0.8.0][ssh-bionic]|[0.9.11][vnc-bionic]
Arch Linux|[3.22.30][gtk-arch]|[0.7.5][ssh-arch]|[0.9.11][vnc-arch]
Solus|[3.22.26][gtk-solus]|[0.7.5][ssh-solus]|[0.9.11][vnc-solus]

[gtk-wheezy]: https://packages.debian.org/wheezy/libgtk-3-0
[gtk-jessie]: https://packages.debian.org/jessie/libgtk-3-0
[gtk-stretch]: https://packages.debian.org/stretch/libgtk-3-0
[gtk-sid]: https://packages.debian.org/sid/libgtk-3-0
[gtk-centos]: https://git.centos.org/summary/rpms!gtk3.git
[gtk-precise]: http://packages.ubuntu.com/precise-updates/libgtk-3-0
[gtk-trusty]: http://packages.ubuntu.com/trusty-updates/libgtk-3-0
[gtk-utopic]: http://packages.ubuntu.com/utopic-updates/libgtk-3-0
[gtk-vivid]: http://packages.ubuntu.com/vivid/libgtk-3-0
[gtk-xenial]: http://packages.ubuntu.com/xenial/libgtk-3-0
[gtk-zesty]: http://packages.ubuntu.com/zesty/libgtk-3-0
[gtk-bionic]: http://packages.ubuntu.com/bionic/libgtk-3-0
[gtk-arch]: https://www.archlinux.org/packages/extra/x86_64/gtk3
[gtk-fedora]: https://apps.fedoraproject.org/packages/gtk3
[gtk-solus]: https://dev.solus-project.com/source/libgtk-3/

[ssh-wheezy]: https://packages.debian.org/wheezy/libssh-4
[ssh-jessie]: https://packages.debian.org/jessie/libssh-4
[ssh-stretch]: https://packages.debian.org/stretch/libssh-4
[ssh-sid]: https://packages.debian.org/sid/libssh-4
[ssh-centos]: https://git.centos.org/summary/rpms!libssh.git
[ssh-lucid]: http://packages.ubuntu.com/lucid-updates/libssh-4
[ssh-precise]: http://packages.ubuntu.com/precise-updates/libssh-4
[ssh-trusty]: http://packages.ubuntu.com/trusty-updates/libssh-4
[ssh-utopic]: http://packages.ubuntu.com/utopic-updates/libssh-4
[ssh-vivid]: http://packages.ubuntu.com/vivid/libssh-4
[ssh-xenial]: http://packages.ubuntu.com/xenial/libssh-4
[ssh-zesty]: http://packages.ubuntu.com/zesty/libssh-4
[ssh-bionic]: http://packages.ubuntu.com/bionic/libssh-4
[ssh-arch]: https://www.archlinux.org/packages/extra/x86_64/libssh
[ssh-fedora]: https://apps.fedoraproject.org/packages/libssh
[ssh-solus]: https://dev.solus-project.com/source/libssh/

[vnc-wheezy]: https://packages.debian.org/wheezy/libvncserver0
[vnc-jessie]: https://packages.debian.org/jessie/libvncserver0
[vnc-stretch]: https://packages.debian.org/stretch/libvncserver1
[vnc-sid]: https://packages.debian.org/sid/libvncserver1
[vnc-centos]: https://git.centos.org/summary/rpms!libvncserver.git
[vnc-lucid]: http://packages.ubuntu.com/lucid/libvncserver0
[vnc-precise]: http://packages.ubuntu.com/precise-updates/libvncserver0
[vnc-trusty]: http://packages.ubuntu.com/trusty-updates/libvncserver0
[vnc-utopic]: http://packages.ubuntu.com/utopic/libvncserver0
[vnc-vivid]: http://packages.ubuntu.com/vivid/libvncserver0
[vnc-xenial]: http://packages.ubuntu.com/xenial/libvncserver1
[vnc-zesty]: http://packages.ubuntu.com/zesty/libvncserver1
[vnc-bionic]: http://packages.ubuntu.com/bionic/libvncserver1
[vnc-arch]: https://www.archlinux.org/packages/extra/x86_64/libvncserver
[vnc-fedora]: https://apps.fedoraproject.org/packages/libvncserver
[vnc-solus]: https://dev.solus-project.com/source/libssh/