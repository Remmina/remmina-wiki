# How to configure key mapping with the VNC plugin.

Remmina has an hidden feature with which you can configure custom key mappings, this is useful if you need, for example, to map the local *Super* key to the remote macOS *Option* key *(leaving theexisting local *Alt* key to the remote macOS* Command *key map alone.)*

See #1716 for a long discussion about this.

Keys can be mapped according to a configurable map. There is a predefined map called "Map Meta Key" that seems to be exactly what we need for macOS.

Edit your connection profile, under "Basic settings", you will find "Keyboard mapping". Choose "Map Meta Keys".

![image](https://gitlab.com/Remmina/Remmina/uploads/e990c8826e534df01d62ea58589cf192/image.png)

Save and connect.

**Map Meta Keys** is the following map:

```
[Map Meta Keys]
Super_L = Meta_L
Super_R = Meta_R
Meta_L = Super_L
Meta_R = Super_R
```

Other maps, using the same syntax, can be created in the `remmina.keymap` file, located in the same directory as the remmina.pref file, usually `~/.config/remmina/remmina.keymap` Key symbol names can be found on the GDK file [gdk/gdkkeysyms.h](https://gitlab.gnome.org/GNOME/gtk/raw/master/gdk/gdkkeysyms.h).

