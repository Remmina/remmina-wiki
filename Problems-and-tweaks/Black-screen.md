If you have a black screen when connecting to a remote Windows server and you have a recent version of Remmina (>v1.4.8), unset or change the microphone redirection option.

![image](uploads/4509e9948a20ab208e70dfedde3edb4c/image.png)

Looks like in some border cases that string is set to 0 or other wrong values.