The RaspberryPi Raspbian 9 default VNC server is RealVNC. RealVNC only supports 5 propertary security types: 5, 6, 13, 130 and 192.
According to the official RealVNC documentation for the "Authentication" parameter, Authentication=VncAuth is the only scheme that allows direct connections from VNC-compatible Viewer projects from third parties.

In order to change to VncAuth scheme in your Raspbian and set a password to accept connections from Remmina VNC plugin, you have to:
1. Open a SSH session or a terminal window to the raspberry
2. As root, edit the file /root/.vnc/config.d/vncserver-x11
```sudo leafpad /root/.vnc/config.d/vncserver-x11```
3. Add the following lines at the end of the file
```
Authentication=VncAuth
Password=aa8ba782c23aff7e
```
4. Save the file.
5. Restart the VNC server service with
```sudo systemctl restart vncserver-x11-serviced```

The above encrypted password is **remmina**, if you want to change it, type
```sudo vncpasswd -service``` and restart the VNC server service with ```sudo systemctl restart vncserver-x11-serviced```



References:

[1] IANA: [Remote Framebuffer Security Types](https://www.iana.org/assignments/rfb/rfb.xhtml#rfb-1)  
[2] RealVNC: [VNC Server parameter reference](https://www.realvnc.com/en/connect/docs/server-parameter-ref.html)


