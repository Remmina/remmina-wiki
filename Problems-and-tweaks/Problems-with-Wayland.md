* Composing a indicator menu with 250-300 elements takes a lot of time (up to 2-3 seconds some times). This composition is done outside Remmina: After Remmina sets the menu with app_indicator_set_menu() and builds it, Remmina returns to the event loop. Then something external to Remmina freezes the mouse cursor for 1-3 seconds when building that menu. Tested on ubuntu 18.04 with Wayland.
* Remmina is not receiving clipboard owner-changes when it doesn't have focus: https://gitlab.com/Remmina/Remmina/issues/2084
* The Windows/Super key cannot be captured (to be verified).
