Sometimes, when you start Remmina, it shows squares instead of each character in all of Remmina' GUI elements.

![image](uploads/05b5a44815444cabe7912831010f0c48/image.png)

This is caused by a broken font cache.

The quick-fix is:

```
pkill remmina
rm -rf ~/.cache/fontconfig
fc-cache -f -v
sudo rm -rf /var/cache/fontconfig/*
sudo fc-cache -f -v
``` 

Start Remmina again and the problem should have disappeared.
