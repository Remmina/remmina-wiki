TL;DR all the RDP remote desktops are blurred on a HiDPI monitor with scaling enabled. And there is no way to fix it under Gnome Shell. Read issues #1778 #1878 to find a workaround for gnome shell, or switch to KDE desktop.

## How is it supposed to work

The RDP protocol, starting with Windows 8.1, requires that scaling be done at the server-side. Your local Remmina client should simply draw unscaled the bitmaps it receives from the server.

## Why it does not work

GTK3 and Gnome Shell have a horrible implementation of HiDPI scaling.
When you enable scaling on your Gnome Shell settings, you enable an integer scaling factor, i.e. 1x 2x 3x. After enabling scaling under Gnome Shell, GTK 3 will scale all bitmaps we draw to a GtkDrawingArea by that scaling factor. It will also scale all cursor bitmaps and mouse positions/movement.
As we told before, RDP requires that all those scaling operations to be done at the server-side by windows, not by the client.

We tried to neutralize GTK integral scaling factor for Remmina application, but we did not reach acceptable results:

* Neutralize bitmap scaling: can be done in code with cairo, but requires some dozen lines of code in various Remmina parts
* Neutralize cursor position scaling: can be done just by multiplying by the scaling factor, but we lose precision in mouse position.
* Neutralize window dimension/allocation scaling: again this can be done by multiplying or dividing by the scaling factor the GtkDrawingArea width and height. But we lose precision.
* Neutralize cursor scaling: seems to be impossible to do with GTK. So we could scale down the cursor bitmap, but it will be shown pixelated due to the duplication of pixels done by GTK.

All this effort took around 100 lines of code, 3-4 hours of development and many hours of testing. And the result is really bad: cursor is still bad, code became more complex, a bit of mouse precision is lost.
Note that the first problem "neutralize bitmap scaling" could be also partially solved using the current Remmina scaler and a fix remote resolution, provided that every time you change the remote resolution you should recalculate your local window size. A simpler approach is to enable the Remmina scaler, set the remote resolution as your monitor full resolution and use Remmina full screen only. But cursors are still not scaled See [issue 1878](https://gitlab.com/Remmina/Remmina/issues/1878).

## Even worse: fractional scaling experimental feature

Recently an experimental feature has been added to Gnome Shell to enable fractional scaling (i.e: 1.7x), both for X.Org and Wayland. This feature seems to set GTK scaling to 2x and then apply a global screen scaling via Xrandr or similar.
This makes Remmina impossible to correctly deal with local scaling: Remmina can no longer disable scaling done by Xrandr, and is also unable to determine your current monitor scaling factor to send to the remote server. The result is very blurry.

## How should it work

Windows 10, MacOS, KDE/Qt have a simple approach to HiDPI: an application can declare itself to be HiDPI aware. When the application declares itself HiDPI aware, all graphical drawings to the monitor are not scaled, all pointer motions are not scaled, all mouse cursor are not scaled. But all wigets of the application, like buttons, texts are still correctly rendered at bigger dimensions. Under Qt this can obtained with a single line of code: `QGuiApplication::setAttribute(Qt::AA_DisableHighDpiScaling);`
Unfortunately, Remmina is not a Qt application, it's a GTK application. And Gnome Shell is not Plasma.

