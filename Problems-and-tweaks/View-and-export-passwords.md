# Viewing passwords

It is possible to view passwords with the [Remmina password exporter](https://pypi.org/project/remmina-password-exposer/), or as per one of the solutions in [Ubuntu Questions](https://askubuntu.com/questions/290824/how-to-extract-saved-password-from-remmina).

# Exporting passwords

It is possible to export passwords from mRemoteNG to Remmina using the [Remmina-mRemoteNG converter](https://github.com/mkud/Remmina_mRemoteNG_conv) Python script.
