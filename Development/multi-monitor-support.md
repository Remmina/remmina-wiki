# Multi monitor support

See also #6 and #323

## Windows server support

- Only in full scren

### WINDOWS 2003 till WINDOWS 2008 (XP, Vista)

- Only using span. One big bitmap for all monitors
- Going out of fullscreen we still have a huge bitmap as the server cannot be reconfigured.
- Removing a montor is not possible, we will have a window with the huge bitmap.
- As conswquence we can go back to fullscreen with any impact.

### WINDOWS 2008R2, WINDOWS 2012 (Win7, Win8)

- First basic, real multi monitor support, the server is aware of each monitor and places the windows correctly.
- Yet, it doesn't support the configuration change awareness.
- Same behaviors as above.

### WINDOWS 2012R2, WINDOWS 2016 (Win 8.1, Win 10) and forward

"Display Control Virtual Channel Extension" has been implemented, https://msdn.microsoft.com/en-us/library/dn366738.aspx

Every time we add qand remove a monitor, the remote server adapt its configuration.

## FreeRDP

- FreeRDP/client/common/client.c

```c
/* Moved logic for Multimon and Span monitors to force fullscreen, so
 * that the rdp file also triggers this functionality */             
if (settings->SpanMonitors)                                          
{                                                                    
    settings->UseMultimon = TRUE;                                
    settings->Fullscreen = TRUE;                                 
}                                                                    
else if (settings->UseMultimon)                                      
{                                                                    
    settings->Fullscreen = TRUE;                                 
}                                                                    
```

