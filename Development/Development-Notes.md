Use this page to add useful links and notes to help out with the development

# Remmina generated documentation

https://daguniverse.github.io/RemminaDoc/index.html

# GTK3

## GTK3 Deprecation mini guide

* [GTK3 Reference Manual](https://developer.gnome.org/gtk3/unstable/index.html)
* [Icon names standard](http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html#names)
* [GTK Stock Item Deprecation](https://docs.google.com/document/d/1KCVPoYQBqMbDP11tHPpjW6uaEHrvLUmcDPqKAppCY8o/pub)
* [Stock Items Migration Guide](https://docs.google.com/spreadsheet/pub?key=0AsPAM3pPwxagdGF4THNMMUpjUW5xMXZfdUNzMXhEa2c&output=html)
* Menu and GtkActionGroup deprecation: [what to use instead](http://stackoverflow.com/questions/24788045/gtk-action-group-new-and-gtkstock-what-to-use-instead)
* _"GtkAlignment has been deprecated in 3.14 and should not be used in newly-written code. The desired effect can be achieved by using the “halign”, “valign” and “margin” properties on the child widget."_ In Remmina GtkAlignment has been replaced by a GtkAspectFrame, used by the "scaler" code to scale the remote desktop.
* Icons in menu are being removed. See [here](https://igurublog.wordpress.com/2014/03/22/gtk-3-10-drops-menu-icons-and-mnemonics/). Also mnemonics should be removed.

