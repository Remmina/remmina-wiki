Remmina lacks good usage and development documentation.

## Usage documentation

In the future we would like to have an offline user manual generated out of the wiki, therefore it's important to get a useful and updated wiki ASAP.

Contribute to the wiki you either editing an existing page, or creating new ones.

To create a new page, click [New page](https://gitlab.com/Remmina/Remmina/-/wikis/new), in the title specify the relative path of the page you want to create.

For instance, if you want to create a page on "How to share your local printers" under the Usage section, you will set the title as `Usage/How-to-share-your-local-printers`.

When you are done, update the sidebar links https://gitlab.com/Remmina/Remmina/-/wikis/_sidebar and the section page (e.g. https://gitlab.com/Remmina/Remmina/-/wikis/Usage).

That's all!

If you prefer to work from a terminal, the wiki can be [cloned](https://gitlab.com/Remmina/Remmina/-/wikis/git_access)

## Developer documentation

We try to document the code as much as we can, but often lack the time and/or some quick-fixes would require documenting a large amount of code.

If you are writing new code, try to document it. If you are not a developer, but would like to learn, documenting the code is hard, but an extremely effective way to learn.

We use [Doxygen](https://www.doxygen.nl/index.html), the documented code is automatically deployed on https://remmina.gitlab.io/remminadoc.gitlab.io/ , a good example, if you don't have Doxygen experience is https://gitlab.com/Remmina/Remmina/-/blob/master/src/remmina_stats.c, that renders as https://remmina.gitlab.io/remminadoc.gitlab.io/remmina__stats_8c.html

Don't be afraid and/or lazy and ask for help in our IRC channel if you have troubles.