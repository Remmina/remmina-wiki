# Testing under Ubuntu with the remmina-next-daily PPA

Remmina users using Ubuntu and derivatives are warmly invited to use our **remmina-next** PPA, as explained in the [Remmina wiki homepage](https://github.com/FreeRDP/Remmina/wiki).
The **remmina-next** PPA is updated every official [release](https://github.com/FreeRDP/Remmina/releases), therefore it can be considered **stable**.

We also provide an **unstable** PPA, called **remmina-next-daily**. remmina-next-daily is updated daily and contains the latest patches pushed to the _next_ github branch.

If you want to help testing Remmina before an official release, you can switch to the remmina-next-daily by doing the following:


1. Install the ppa-purge package
```sudo apt install ppa-purge```
2. Purge the remmina-next PPA
```sudo ppa-purge ppa:remmina-ppa-team/remmina-next```
3. Install the remmina-next-daily PPA
```
sudo apt-add-repository ppa:remmina-ppa-team/remmina-next-daily
sudo apt update
```
4. Install Remmina packages

* if not installed previously
```
sudo apt install remmina remmina-plugin-rdp libfreerdp-plugins-standard
```

* if installed previously, just do: 
```
sudo apt upgrade
```
You might need to perform the following command to remove the previous Remmina from memory:
```
sudo killall remmina
```


Happy testing :)

