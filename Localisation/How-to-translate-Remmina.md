Please follow the [CONTRIBUTING guide](https://gitlab.com/Remmina/Remmina/blob/master/CONTRIBUTING.md) for general info on how to contribute to Remmina.

Remmina uses xgettext to generate the translated strings, you don't necessarily need to have a good understanding of it but in the case you need an high level overview check [this](https://www.gutenberg.org/wiki/Gutenberg:GNU_Gettext_Translation_How-To)

# Using Weblate

[![Translation status](https://hosted.weblate.org/widgets/remmina/-/remmina/287x66-black.png)](https://hosted.weblate.org/engage/remmina/?utm_source=widget)

- [Translate](https://hosted.weblate.org/projects/remmina/)
- [View project languages](https://hosted.weblate.org/projects/remmina/#languages)

# Using Git and and a text or PO Editor


## To add a new language

Download the [remmina.pot](https://gitlab.com/Remmina/Remmina/blob/master/po/remmina.pot) and using a PO editor (Poedit, Gtranslator, …) create the PO file of your language.

When you are done send it to l10n@lists.remmina.org , alternatively you can fork the Remmina repository and submit a merge request.

## To update an existing translation.

All the existing po files are under https://gitlab.com/Remmina/Remmina/blob/master/po

Grab the one you want to update and use a PO editor to update it, than you can either send to us at l10n@lists.remmina.org, either submit a merge request.

## Translations status 
 
[![Translation status](https://hosted.weblate.org/widgets/remmina/-/remmina/horizontal-auto.svg)](https://hosted.weblate.org/engage/remmina/?utm_source=widget)
