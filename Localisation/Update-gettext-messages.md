This is a quick, dirty and no brainy procedure to update the PO files in Remmina.

Please wait 10 seconds before copying and pasting it in the terminal.

```bash
REMMINATOP=~/remmina_devel/Remmina 
cd $REMMINATOP 
find src plugins -name "*.c" | sed 's/^.\///' >| po/POTFILES.in 
find data -name "*.glade" | sed 's/^.\///' >> po/POTFILES.in 
find data -name "*.appdata.xml" | sed 's/^.\///' >> po/POTFILES.in
xgettext --from-code=UTF-8 -k_ -kN_ -ktranslatable --keyword=C_:1c,2 --keyword=NC_:1c,2 --keyword=g_dngettext:2,3 --add-comments --files-from=po/POTFILES.in --output=po/messages.pot --copyright-holder="2014–2020 Weblate translators" --package-name="Remmina" --package-version="1.2.31" --msgid-bugs-address="admin@remmina.org"
cd $REMMINATOP/po 
for i in *.po; do  
msgmerge -N --backup=off --update $i messages.pot 
done 
for i in $REMMINATOP/po/*.po ; do  
TMPF=/tmp/f$$.txt 
sed '/^#~/d' $i > $TMPF 
awk 'BEGIN{bl=0}/^$/{bl++;if(bl==1)print;else next}/^..*$/{bl=0;print}' $TMPF >| $i 
rm $TMPF 
done 
rm *.po~ 
rm messages.pot 

```

To test Remmina in another language, you can start it from the commandline this way:

```bash
LANGUAGE=ru remmina
``` 
